# NoArtifactLights
![AppVeyor](https://img.shields.io/appveyor/build/RelaperCrystal/NoArtifactLights?style=flat-square)

NoArtifactLights is a GTA5 survival mod based on Blackouts and NPCs fights eachother.

## Building
Clone the repository. Once done, use `devenv NoArtifactLights.sln /Build Release` to build release version which should act like same as release version. If you wish to, you can use `devenv NoArtifactLights.sln /Build Debug` to build these unstable features. 

You can also use Visual Studio IDE to build. Simply open Visual Studio, clone the repository, and select Debug or Release, then BUILD.
